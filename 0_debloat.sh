#!/bin/bash

YELLOW='\e[1;33m'
RESET='\e[0m'
# I don't know how this # is necessary :|
info_msg(){
	echo -e "${YELLOW} [[[ $1 ]]] ${RESET}"
}

# WARNING!
# If you are uninstalling ALL terminals, please remove the "#" from the next line. (And replace "kitty" for your terminal if you want)
sudo pacman -S kitty --needed

uninstall_packages+="gwenview okular kate dolphin dolphin-plugins partitionmanager kcalc spectacle ksystemlog yakuake konsole "

# Optional [UNINSTALL]
uninstall_packages+="kdeconnect "

info_msg "UNINSTALLING PACKAGES"
sudo pacman -Rsu ${uninstall_packages}