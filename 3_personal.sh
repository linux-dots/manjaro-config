#!/bin/bash

YELLOW='\e[1;33m'
RESET='\e[0m'
# I don't know how this # is necessary :|
info_msg(){
	echo -e "${YELLOW} [[[ $1 ]]] ${RESET}"
}

PACKAGES+="neofetch telegram-desktop libreoffice nmap keepassxc audacity obs-studio docker docker-compose yay noto-fonts-emoji "

# Sometimes...
PACKAGES+="flatpak "

info_msg "INSTALLING YOUR PACKAGES"
sudo pacman -Sy --needed $PACKAGES