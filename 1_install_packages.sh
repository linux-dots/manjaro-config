#!/bin/bash

YELLOW='\e[1;33m'
RESET='\e[0m'
# I don't know how this # is necessary :|
info_msg(){
	echo -e "${YELLOW} [[[ $1 ]]] ${RESET}"
}

# Optional [UPDATE SYSTEM]
#sudo pacman -Syu

# Optional
install_packages+="leafpad xviewer thunar gparted gnome-calculator flameshot catfish gnome-system-monitor kitty gvfs rofi "

# Optional
info_msg "INSTALLING PACKAGES"
sudo pacman -Sy ${install_packages} --needed