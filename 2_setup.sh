#!/bin/bash

YELLOW='\e[1;33m'
RESET='\e[0m'
# I don't know how this # is necessary :|
info_msg(){
	echo -e "${YELLOW} [[[ $1 ]]] ${RESET}"
}

info_msg "CHANGING COMMAND INTERPRETER"
chsh -s /usr/bin/zsh

info_msg "COPYING CONFIG FILES TO $HOME/.config/"
cp -r .config/* ~/.config/

info_msg "MAKING XDG_DIRS"
xdg-user-dirs-update
sleep 2
source ~/.config/user-dirs.dirs

info_msg "MAKING BOOKMARKS FOR THUNAR"
if [ -f ~/.config/gtk-3.0/bookmarks ]; then
	rm ~/.config/gtk-3.0/bookmarks
fi

echo "file://$XDG_DOWNLOAD_DIR" > ~/.config/gtk-3.0/bookmarks
echo "file://$XDG_DOCUMENTS_DIR" >> ~/.config/gtk-3.0/bookmarks
echo "file://$XDG_PICTURES_DIR" >> ~/.config/gtk-3.0/bookmarks
echo "file://$XDG_MUSIC_DIR" >> ~/.config/gtk-3.0/bookmarks
echo "file://$XDG_VIDEOS_DIR" >> ~/.config/gtk-3.0/bookmarks

info_msg "CREATING SCREENSHOTS FOLDER IN $XDG_PICTURES_DIR/Screenshots"
mkdir -p $XDG_PICTURES_DIR/Screenshots

sleep 2
info_msg "FIXING FLAMESHOT PATH"
sed -i "s|~/Images/Screenshots/|$XDG_PICTURES_DIR/Screenshots/|g" ~/.config/khotkeysrc

sleep 2
info_msg "PINNING PROGRAMS TO TASK BAR"
PINNED_APPS="applications:firefox.desktop,applications:kitty.desktop,applications:thunar.desktop"
sed -i 's|^launchers=.*|launchers='"$PINNED_APPS"'|g' ~/.config/plasma-org.kde.plasma.desktop-appletsrc
